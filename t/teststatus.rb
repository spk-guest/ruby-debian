require 'test/unit'
require 'debian'

class TestDebian__Status < Test::Unit::TestCase
  
   def test_s_new
     s = Debian::Status.new(['dpkg'])
     assert((s['dpkg'].data.find {|f| f == "/usr/bin/dpkg" }) != nil)
   end
#
#  def test_s_parse
#    assert_fail("untested")
#  end
#
#  def test_s_parseAptLine
#    assert_fail("untested")
#  end

#  def test_s_parseArchiveFile
#    assert_fail("untested")
#  end

end
